# build Custom OrangePI PC2 Boot


build for ORangepi PC2 source from [github/megous orange-pi-5.2](https://github.com/megous/linux/blob/orange-pi-5.2/README.md)

build from sources: 

1. [arm-trusted-firmware](https://github.com/ARM-software/arm-trusted-firmware)
2. [megous u-boot opi-v2019.07](https://megous.com/git/u-boot/tree/opi-v2019.07)
3. [megous linux orange-pi-5.3](https://github.com/megous/linux/tree/orange-pi-5.3)
4. [megous firmware](https://megous.com/git/linux-firmware)
5. [arm64-linux-gcc-8.2](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-a/downloads)

## for use armbian

with single ext4 partition, download from [armbian](https://www.armbian.com/orange-pi-pc2/)

and dd to sd card:

`sudo dd if=Armbian_5.90_Orangepipc2_Ubuntu_bionic_next_4.19.57_desktop.img  of=/dev/disk3 bs=4M status=progress`


## Install this output



* `uboot.bin` (spl/sunxi-spl.bin & uboot.itb)

install as `dd if=uboot.bin of=/dev/<mydev> bs=8k seek 1 `

* `uImage` the kernel
* `board.dtb` the device tree

copy to `/boot` @ ext4 partition 

### additional Output

* `headers.tar.xz` : linux headers => `/lib/modules`
* `modules.tar.xz` : linux additional modules => `/usr/include`

extract to `/`

### config Output

* `linux.config` full linux `.config` as used for this build
* `uboot.config` full u-boot `.config` as used for this build
* `opi_pc2_smooty_defconfig` the default kernel config for this device

