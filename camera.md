# dts overlay study camera on CSI


## From sun50i-h5-orangepi-pc2:

* `/arch/arm64/boot/dts/allwinner/sun50i-h5-orangepi-pc2.dts`
    * included `sun50i-h5.dtsi`
        * included `arm/sunxi-h3-h5.dtsi`
```dtsi
csi: camera@1cb0000 {
			compatible = "allwinner,sun8i-h3-csi";
			reg = <0x01cb0000 0x1000>;
			interrupts = <GIC_SPI 84 IRQ_TYPE_LEVEL_HIGH>;
			clocks = <&ccu CLK_BUS_CSI>,
				 <&ccu CLK_CSI_SCLK>,
				 <&ccu CLK_DRAM_CSI>;
			clock-names = "bus", "mod", "ram";
			resets = <&ccu RST_BUS_CSI>;
			pinctrl-names = "default";
			pinctrl-0 = <&csi_pins>;
			status = "disabled";
		};

i2c2: i2c@1c2b400 {
			compatible = "allwinner,sun6i-a31-i2c";
			reg = <0x01c2b400 0x400>;
			interrupts = <GIC_SPI 8 IRQ_TYPE_LEVEL_HIGH>;
			clocks = <&ccu CLK_BUS_I2C2>;
			resets = <&ccu RST_BUS_I2C2>;
			pinctrl-names = "default";
			pinctrl-0 = <&i2c2_pins>;
			status = "disabled";
			#address-cells = <1>;
			#size-cells = <0>;
		};

```


## Implementation camera for `sun8i-a83t-tbs-a711`

`arch/arm/boot/dts/sun8i-a83t-tbs-a711.dts`

```

&csi {
	pinctrl-names = "default";
	pinctrl-0 = <&csi_8bit_parallel_pins>, <&csi_mclk_pin>;
	status = "okay";
};

&csi_in {
	csi_hm5065_ep: endpoint {
		remote-endpoint = <&hm5065_ep>;
		bus-width = <8>;
		data-active = <1>;
		pclk-sample = <0>;
		hsync-active = <0>;
		vsync-active = <1>;
	};
};

&i2c_gpio {
	hm5065: camera@1f {
		compatible = "himax,hm5065";
		reg = <0x1f>;
		clocks = <&ccu CLK_CSI_MCLK>;
		clock-names = "xclk";
		IOVDD-supply = <&reg_dldo3>;
		AVDD-supply = <&reg_dldo4>;
		DVDD-supply = <&reg_eldo3>;
		AFVDD-supply = <&reg_dldo3>;
		reset-gpios = <&pio 4 18 GPIO_ACTIVE_LOW>; /* PE18 */
		enable-gpios = <&pio 4 19 GPIO_ACTIVE_HIGH>; /* PE19 */

		port {
			hm5065_ep: endpoint {
				remote-endpoint = <&csi_hm5065_ep>;
				bus-width = <8>;
				data-active = <1>;
				pclk-sample = <0>;
				hsync-active = <0>;
				vsync-active = <1>;
			};
		};
	};
};
```





## for nanopi with hxx [patch/kernel/sunxi-dev/board-nanopiair-h3-camera-wifi-bluetooth-otg.patch](https://github.com/armbian/build/commit/10e206519089b8ff81b0dd6a6f3caa6c1adba04d#diff-5ec21c7daf434027694931230901ca60R96)

```
&csi {
    status = "okay";

    port {
        #address-cells = <1>;
        #size-cells = <0>;

        /* Parallel bus endpoint */
        csi_from_ov5640: endpoint {
            remote-endpoint = <&ov5640_to_csi>;
            bus-width = <8>;
            data-shift = <2>;
            hsync-active = <1>; /* Active high */
            vsync-active = <0>; /* Active low */
            data-active = <1>;  /* Active high */
            pclk-sample = <1>;  /* Rising */
        };
    };
};

&i2c2 {
   status = "okay";
   ov5640: camera@3c {
        compatible = "ovti,ov5640";
        reg = <0x3c>;
        clocks = <&cam_xclk>;
        clock-names = "xclk";

        reset-gpios = <&pio 4 14 GPIO_ACTIVE_LOW>;
        powerdown-gpios = <&pio 4 15 GPIO_ACTIVE_HIGH>;
        AVDD-supply = <&reg_cam_avdd>;
        DOVDD-supply = <&reg_cam_dovdd>;
        DVDD-supply = <&reg_cam_dvdd>;

        port {
            ov5640_to_csi: endpoint {
                remote-endpoint = <&csi_from_ov5640>;
                bus-width = <8>;
                data-shift = <2>;
                hsync-active = <1>; /* Active high */
                vsync-active = <0>; /* Active low */
                data-active = <1>;  /* Active high */
                pclk-sample = <1>;  /* Rising */
            };
        };
    };
};

&i2c2_pins {
   bias-pull-up;
};
```

## New overlay:

Need:
1. csi to set status okay
2. create csi in
3. set i2c2 to okay
4. add compatiple ov5640 camera



compile `SSL_CAM_OV5640.dts` with:

```bash
dtc -O dtb -I dts -o SSL_CAM_OV5640.dtbo -b 0  SSL_CAM_OV5640.dts
```

check new dtbo:

```bash

fdtdump SSL_CAM_OV5640.dtbo

```

create new board dtb:

```bash
fdtoverlay -i /boot/board.dtb -o /boot/bwSSL_CAM.dtb -v /boot/overlay/SSL_CAM_OV5640.dtbo
```

```bash
setenv load_addr "0x40000000"

# Print boot source
itest.b *0x10028 == 0x00 && echo "U-boot loaded from SD"
itest.b *0x10028 == 0x02 && echo "U-boot loaded from eMMC or secondary SD"
itest.b *0x10028 == 0x03 && echo "U-boot loaded from SPI"

echo "Boot script loaded from ${devtype}"

setenv bootargs 'root=/dev/mmcblk0p1 rw rootwait console=tty0 console=ttyS0,115200n8 rootfs=ext4 noinitrd selinux=0 splashimage=(mmc0,1)/boot/boot.bmp'

ext4load mmc 0:1 ${kernel_addr_r} /boot/uImage
ext4load mmc 0:1 ${fdt_addr_r} /boot/bwSSL_CAM.dtb
bootm ${kernel_addr_r} - ${fdt_addr_r}

# Recompile with:
# mkimage -C none -A arm -T script -d boot.cmd boot.scr
```


list flat tree:

```bash
fdtdump /boot/bwSSL_CAM.dtb
```

list from running device:

```bash
dtc -I fs /proc/device-tree
```

check overlay:

```
cat /proc/device-tree/chosen/overlays/SSL_CAM_OV5640
```

current status:

```
[    9.342794] i2c i2c-1: mv64xxx: I2C bus locked, block: 1, time_left: 0
[    9.342808] ov5640 1-003c: ov5640_read_reg: error: reg=300a
[    9.342813] ov5640 1-003c: ov5640_check_chip_id: failed to read chip identifier
[    9.343007] ov5640: probe of 1-003c failed with error -110

checking i2c

https://groups.google.com/forum/#!topic/linux-sunxi/RDPRFgUx0KY

Turn power on:

```
#stdby off
sudo sunxi-pio -m "PE15<1><0><1><0>"
# CSI_POWER on 
sudo sunxi-pio -m "PA17<1><0><1><1>"
# AFC power on
sudo sunxi-pio -m "PG13<1><0><1><1>"
# reset
sudo sunxi-pio -m "PE14<1><0><1><0>"
sudo sunxi-pio -m "PE14<1><0><1><1>"
```

| function | pin | GPIO cmd |
|---|---|---|
| AFCC_EN | PG13 | <0xa 0x6 0xd 0x0> |
| CSI_POWER_EN | PA17 | <0xa 0x0 0x11 0x0> |
| CSI_STDB_EN | PE15 | <0xa 0x4 0xf 0x0> |
| CSI_RESET# | PE14 | <0xa 0x4 0xe 0x1> |


`sudo apt install i2c-tools`

i2cdetect -l

i2cdetect -y 2
i2cdetect -y 1

## display video

```bash
mplayer -fs tv:// -tv driver=v4l:width=352:height=288
```
or

```bash
mplayer tv:// -tv driver=v4l2:width=640:height=480:device=/dev/video0:fps=30:outfmt=yuy2
```

```
cvlc --no-audio v4l2:///dev/video0:width=640:height=480
```

FROM: [linux questions: adjusting-v4l2-settings-after-launching-mplayer-to-framebuffer-in-console](https://www.linuxquestions.org/questions/linux-software-2/adjusting-v4l2-settings-after-launching-mplayer-to-framebuffer-in-console-4175493692-print/)

stream with `mplayer -tv driver=v4l2:device=/dev/video0 tv://` and mplayer resets the v4l2 settings e.g. `v4l2-ctl --set-ctrl brightness=134`

Mplayer can be set into a slave mode where you can feed it more commands. Among those commands is the "run" command which can run a terminal command enclosed in quotation marks. You can also direct commands into mplayer from a file. Combined, these two features lead to a solution:

create a file with the mplayer commands you wish to give. My file is called "v4l-mplayer-settings". The file consists of the following:
Code:
```bash
run "v4l2-ctl --set-ctrl brightness=134"
run "v4l2-ctl --set-ctrl contrast=43"
run "v4l2-ctl --set-ctrl saturation=38"
```

Then I run this command in the terminal (or from a bash script):
Code:
```bash
mplayer -vo sdl:driver=fbcon -slave -quiet -input file=v4l-mplayer-settings -tv driver=v4l2:device=/dev/video0 tv://
```
the `-vo` flag and the following `sdl:driver=fbcon` may be particular to my system, as a lot of people suggest using `fbdev` instead of `fbcon`. This sends your video to the framebuffer. It may be optional to set this if you are running the mplayer command from the console without any x software running.

`-slave` puts mplayer in slave mode to accept commands
`-quiet` is meant to provide you an mplayer-specific command line where you can feed mplayer commands via the keyboard. I'm not sure if it's required if you use `-input` file. However, it doesn't hurt.
`-input file`= is where I direct mplayer to use the commands I included in the above v4l-mplayer-settings file.
`-tv` indicates that we are setting properties for the tv device, using video for linux 2 (which is what I need to use to access my webcam), and the device at /dev/video0 (which is the video device of my webcam - your system may be different.
Finally, `tv://` just directs mplayer to invoke its TV viewer system.

## use v4l2 api

[Capture images using V4L2 on Linux](https://jayrambhia.com/blog/capture-v4l2) source [capturev4l2.c](https://gist.github.com/jayrambhia/5866483)

## i2c

from [sunxi h5 DT overlays](https://github.com/armbian/sunxi-DT-overlay)


