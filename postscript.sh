#!/bin/bash
. trap_print
cd $BASE/Toolchains
ls -la
tar -xJf gcc-arm-8.2-2019.01-x86_64-aarch64-linux-gnu.tar.xz
ls -la
mkdir -pv "${target}/opt/Toolchains"
mv -v gcc-arm-8.2-2019.01-x86_64-aarch64-linux-gnu "${target}/opt/Toolchains/aarch64"
cd $BASE
echo -e "\n/opt/Toolchains/aarch64/lib64\n" >> "${target}/etc/ld.so.conf"
/usr/sbin/ldconfig -r "${target}" -f "/etc/ld.so.conf" -C "/etc/ld.so.cache"
ls -la ${target}/opt/Toolchains/aarch64/bin
cp -vf /usr/sbin/trap_print $target/usr/sbin/trap_print
 